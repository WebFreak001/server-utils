# mcserv

```
Usage: mcserv <server> [start/stop/status/kill/restart/run <cmd>]

Available servers:
fitp (runs /home/minecraft/FireInThePipe/LaunchServer.sh)
vanilla112 (runs /home/minecraft/Vanilla1.12/start.sh)
```

Wrapper utility around `screen` to launch and stop minecraft servers. Creates screens with the name `mcs_<name>`, loads available servers from configured config dir (`~/.config`) from `mcserv/servers.json` in the form of

```json
{
	"servername": "/path/to/.sh_or_fish_or_jar"
}
```