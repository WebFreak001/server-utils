import scriptlike;
import standardpaths;
import std.json;
import std.process;
import core.thread;

int main(string[] args)
{
	auto configDirs = standardPaths(StandardPath.config, "mcserv");
	string[string] servers;
	foreach (configDir; configDirs)
	{
		auto configFile = buildPath(configDir, "servers.json");
		if (configFile.exists)
		{
			try
			{
				auto json = parseJSON(configFile.readText);
				enforce(json.type == JSON_TYPE.OBJECT);
				foreach (string key, JSONValue value; json)
				{
					if (value.type == JSON_TYPE.STRING)
					{
						auto path = expandTilde(value.str);
						servers[key.strip.toLower] = path;
						if (!path.exists)
						{
							writeln("Warning: Path ", path, " specified in config from ",
									configDir, " does not exist!");
						}
					}
					else
						writeln("Warning: Invalid value ", value, " in config from ", configDir);
				}
			}
			catch (Exception e)
			{
				writeln("Warning: Failed to read servers.json from ", configDir);
			}
		}
	}
	if (args.length <= 2)
	{
		printUsage(args[0], servers);
		return 2;
	}
	string server = args[1].strip.toLower;
	string action = args[2];
	auto pathPtr = server in servers;
	if (!pathPtr)
	{
		writeln("Server does not exist!");
		return 1;
	}
	bool running = server.isRunning;
	switch (action.toLower.strip)
	{
	case "start":
		if (running)
		{
			writeln("Server already running");
			return 1;
		}
		startServer(server, *pathPtr);
		return 0;
	case "stop":
		if (!running)
		{
			writeln("Server not running");
			return 1;
		}
		runCommand(server, "stop");
		break;
	case "status":
		writeln("Server ", server);
		writeln("Path: ", *pathPtr);
		writeln("Status: ", running ? "running" : "not running");
		if (running)
			return 0;
		else
			return 1;
	case "kill":
		if (!running)
		{
			writeln("Server not running");
			return 1;
		}
		if (!killScreen(server))
		{
			writeln("Failed to kill server - kill manually please");
			return 1;
		}
		return 0;
	case "restart":
		if (!running)
		{
			writeln("Server not running");
			return 1;
		}
		writeln("Stopping server...");
		runCommand(server, "stop");
		int deadCounter = 0;
		while (true)
		{
			Thread.sleep(1.seconds);
			if (!server.isRunning)
				break;
			deadCounter++;
			if (deadCounter >= 30)
			{
				writeln("Could not stop server, aborting");
				return 0;
			}
		}
		writeln("Starting server...");
		startServer(server, *pathPtr);
		break;
	case "run":
		if (!running)
		{
			writeln("Server not running");
			return 1;
		}
		if (args.length <= 3)
		{
			printUsage(args[0], servers);
			return 2;
		}
		runCommand(server, args[3 .. $].join(' '));
		break;
	default:
		printUsage(args[0], servers);
		return 2;
	}
	return 0;
}

void startServer(string server, string path)
{
	string dir = dirName(path);
	string ext = extension(path);
	writeln("Extension: ", ext);
	if (ext == ".jar")
		startScreen(server, dir, ["java", "-Xmx2G", "-XX:MaxPermSize=256M", "-jar", path, "nogui"]);
	else if (ext == ".fish")
		startScreen(server, dir, ["fish", path]);
	else if (ext == ".sh")
		startScreen(server, dir, ["sh", path]);
	else
		startScreen(server, dir, [path]);
	writeln("Starting up server. Use `screen -x mcs_", server,
			"` to attach to the console and use Ctrl-A D to detatch");
}

bool isRunning(string sessionName)
{
	auto ret = myExecute(["screen", "-S", "mcs_" ~ sessionName, "-Q", "select", "."]);
	return ret.status == 0;
}

auto runCommand(string sessionName, string command)
{
	return mySpawnProcess(["screen", "-S", "mcs_" ~ sessionName, "-X", "stuff", command ~ '\n']);
}

bool killScreen(string sessionName)
{
	auto ret = myExecute(["screen", "-S", "mcs_" ~ sessionName, "-X", "quit"]);
	return ret.status == 0;
}

auto startScreen(string sessionName, string cwd, string[] command)
{
	writeln("Starting server in ", cwd);
	return mySpawnProcess(["screen", "-dmS", "mcs_" ~ sessionName] ~ command,
			stdin, stdout, stderr, null, Config.none, cwd);
}

auto myExecute(Args...)(Args args)
{
	writeln("Running ", args[0].join(' '));
	return execute(args);
}

auto mySpawnProcess(Args...)(Args args)
{
	writeln("Running ", args[0].join(' '));
	return spawnProcess(args);
}

void printUsage(string progName, string[string] servers)
{
	writeln("Usage: ", progName, " <server> [start/stop/status/kill/restart/run <cmd>]");
	writeln();
	writeln("Available servers:");
	foreach (name, path; servers)
		writeln(name, " (runs ", path, ")");
}
