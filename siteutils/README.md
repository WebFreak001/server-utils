# siteutils

Provides `mksite` `lssite` `rmsite`.

Assumes site layout like the following:

```
$ROOT (/srv/http)
	longest.tld.in.reverse/
		site.conf (nginx config)
		public/ (for static sites)
```

Settings in `/etc/siteutils.conf`:

```ini
# root path where to put sites
root=/srv/http

[templates]
# templates for sites (copied and replacing tokens in all text files)
# Tokens:
#   #{TLD} -> replaced with primary TLD
#   #{TLDs} -> replaced with space separated TLDs
#   #{Port} -> replaced with reverse proxy port (only generated if found)
#   #{Name} -> Name of site folder (longest TLD in reverse)
#   #{Path} -> absolute path to site folder without trailing slash
static=/srv/http/templates/static
```