import config;

import std.algorithm;
import std.file;
import std.getopt;
import std.path;
import std.process;
import std.range;
import std.stdio;
import std.string;

int main(string[] args)
{
	string usedTemplate;
	string customRoot;
	string configFile;
	bool listTemplates;

	auto help = getopt(args,
			std.getopt.config.passThrough, "root|r",
			"Override root directory to /srv/http from config", &customRoot, "config|c",
			"Custom path to config file instead of /etc/siteutils.conf",
			&configFile, "list|l", "List all available template folders and names", &listTemplates);

	if (help.helpWanted)
	{
		defaultGetoptPrinter("Creates a nginx site from a template\nUsage: " ~ args[0] ~ " [template] [TLDs...]",
				help.options);
		return 1;
	}

	SiteConfig config;
	if (configFile.length)
		config = loadSiteConfig(configFile);
	else
		config = loadSiteConfig();

	if (customRoot.length)
		config.root = customRoot;

	if (listTemplates)
	{
		writeln("Available templates (", config.templates.length, "):");
		foreach (name, path; config.templates)
			writeln(name, " -> ", path);
		return 0;
	}

	if (args.length <= 2)
	{
		defaultGetoptPrinter("Creates a nginx site from a template\nUsage: " ~ args[0] ~ " [template] [TLDs...]",
				help.options);
		return 1;
	}

	string templ = args[1];

	if (auto repl = templ in config.templates)
		templ = *repl;

	if (!templ.exists)
	{
		if (templ != args[1])
			stderr.writeln("Template path ", templ, " (resolved '", args[1],
					"' from config template) doesn't exist!");
		else
			stderr.writeln("Template path ", templ, " doesn't exist!");
		return 1;
	}

	string[] tlds = args[2 .. $];
	string tld = tlds[0];
	string name = tlds.maxElement!"a.length".splitter('.').retro.join('.').toLower;
	string path = buildPath(config.root, name);
	auto port = Lazy!string(execute(["mkport", "web", tld]).output.strip);

	if (path.exists)
	{
		stderr.writeln("Output path ", path, " already exists!");
		return 1;
	}

	mkdirRecurse(path);
	spawnProcess(["cp", "-rvT", templ, path]).wait;

	foreach (file; dirEntries(path, SpanMode.depth))
	{
		if (file.isFile && file.isText)
		{
			string content = readText(file);
			auto newFile = File(file, "wb");

			ptrdiff_t i = content.indexOf("#{");
			ptrdiff_t last = 0;
			while (i >= 0)
			{
				auto end = content.indexOf('}', i);
				if (end != -1)
				{
					newFile.rawWrite(content[last .. i]);
					auto variable = content[i + "#{".length .. end];
					switch (variable)
					{
					case "TLD":
						newFile.rawWrite(tld);
						break;
					case "TLDs":
						foreach (n, t; tlds)
						{
							if (n != 0)
								newFile.rawWrite(" ");
							newFile.rawWrite(t);
						}
						break;
					case "Port":
						newFile.rawWrite(cast(string) port);
						break;
					case "Name":
						newFile.rawWrite(name);
						break;
					case "Path":
						newFile.rawWrite(path);
						break;
					default:
						stderr.writeln("Warning: Ignoring unknown variable ", variable, " in file ", file);
						newFile.rawWrite(content[i .. end + 1]);
						break;
					}
					i = end + 1;
				}
				else
				{
					newFile.rawWrite(content[last .. i += 2]);
				}
				last = i;
				i = content.indexOf("#{", i);
			}
			if (last != 0)
				stderr.writeln("Substituted file ", file);
			newFile.rawWrite(content[last .. $]);
		}
	}

	writeln("Successfully created site in ", path);
	if (port.evaluated)
	{
		writeln();
		writeln("Port: ", port.get);
		writeln();
		writeln("Don't forget to mkservice to automatically start your service!");
	}
	return 0;
}

/// Guesses if a file is text by checking if the first 1024 bytes are UTF-8
bool isText(string file)
{
	import std.utf : validate;

	foreach (c; File(file, "rb").byChunk(1024))
	{
		// strip off half characters
		foreach (_; 0 .. 3)
			if (c[$ - 1] >= 128)
				c.length--;

		try
		{
			validate(cast(string) c);
			return true;
		}
		catch (Exception)
		{
			return false;
		}
	}

	return false;
}

struct Lazy(T)
{
	T delegate() evaluate;
	T value;
	bool evaluated;

	this(lazy T value)
	{
		evaluate = () => value;
	}

	alias get this;

	T get()
	{
		if (evaluated)
			return value;
		else
		{
			evaluated = true;
			return value = evaluate();
		}
	}
}
