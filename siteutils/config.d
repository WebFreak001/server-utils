module config;

import std.algorithm;
import std.file;
import std.stdio;
import std.string;

struct SiteConfig
{
	string root = "/srv/http";
	string[string] templates;
}

SiteConfig loadSiteConfig(string config = "/etc/siteutils.conf")
{
	SiteConfig ret;
	if (config.exists)
	{
		string section;
		foreach (line; File(config).byLine)
		{
			line = line.strip;
			if (!line.length)
				continue;
			if (line[0] == ';' || line[0] == '#')
				continue;
			if (line[0] == '[' && line[$ - 1] == ']')
			{
				section = line[1 .. $ - 1].idup;
				continue;
			}

			auto _parts = line.findSplit("=");
			auto key = _parts[0];
			auto value = _parts[2];
			if (section.length == 0)
			{
				switch (key)
				{
				case "root":
					ret.root = value.idup;
					break;
				default:
					stderr.writeln(config, ": Unknown setting ", key, " in default section");
					break;
				}
			}
			else if (section == "templates")
			{
				ret.templates[key.idup] = value.idup;
			}
			else
			{
				stderr.writeln(config, ": Setting in unknown section [", section, "]");
			}
		}
	}
	return ret;
}
