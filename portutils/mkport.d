import config;

import std.algorithm;
import std.conv;
import std.datetime.systime;
import std.datetime.timezone;
import std.stdio;
import std.string;
import fs = std.file;

int main(string[] args)
{
	string exeName = args[0];
	args = args[1 .. $];
	PortRange[] ranges;
	foreach (i, arg; args)
	{
		auto range = PortRange.parse(arg);
		if (range != PortRange.init)
			ranges ~= range;
		else
		{
			args = args[i .. $];
			break;
		}
	}

	bool private_;

	while (args.length)
	{
		string arg = args[0];
		if (arg == "--private" || arg == "-private" || arg == "-p")
			private_ = true;
		else
			break;
		args = args[1 .. $];
	}

	if (!args.length)
	{
		stderr.writeln("Usage: ", exeName, " [ports...] {[--private]} [service] {[description]}");
		return 1;
	}

	auto config = loadPortConfig();

	string service = args[0];
	string description = args[1 .. $].join(" ");

	if (ranges.length == 0)
	{
		if (auto map = service in config.servicemaps)
			ranges = *map;
		else if (auto map = "default" in config.servicemaps)
			ranges = *map;
		else
			ranges = PortConfig.defaultServicemap;
	}

	auto f = File(config.map, "r+");

	auto port = ranges.nextPort(config.padding);

	int lineNo = 0;
	size_t offset = 0;
	foreach (line; f.byLine(KeepTerminator.yes))
	{
		size_t length = line.length;
		scope (exit)
			offset += length;

		lineNo++;
		line = line.strip;
		if (!line.length || line[0] == '#')
			continue;

		auto v = PortEntry.parse(line);
		if (v == PortEntry.init)
		{
			stderr.writeln(config.map, "(", lineNo, "): Unparsable line: ", line);
			continue;
		}
		else
		{
			if (port > v.port)
				continue;
			else if (port == v.port)
			{
				auto next = ranges.nextPort(config.padding);
				if (next == 0)
				{
					stderr.writeln("Could not satisfy port range (all ports used already)");
					return 1;
				}
				else if (next <= port)
				{
					stderr.writeln("Port ranges may only be notated in ascending order!");
					return 1;
				}
				port = next;
			}
			else if (port < v.port)
			{
				length = 0;
				break;
			}
		}
	}

	PortEntry line;
	line.port = port;
	line.createTime = Clock.currTime(UTC());
	line.service = service;
	line.public_ = !private_;
	line.description = description;
	f.insert(offset, line.toString ~ "\n");
	f.close();

	writeln(port);

	if (!private_ && config.onPublicPortAdd.length)
	{
		import std.process : spawnProcess, wait;

		spawnProcess([config.onPublicPortAdd, line.port.to!string, line.service, line.description])
			.wait;
	}

	return 0;
}

void insert(ref File file, size_t offset, string s)
{
	file.seek(0, SEEK_END);
	auto end = file.tell();
	file.seek(offset);
	if (offset >= end)
	{
		file.write(s);
	}
	else
	{
		ubyte[] data = new ubyte[end - offset];
		file.rawRead(data);
		file.seek(offset);
		file.write(s);
		file.rawWrite(data);
	}
}
