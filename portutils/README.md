# portutils

Provides `mkport`, `lnport`, `lsport`, `rmport`.

Helps keeping track of all unlocked/assigned ports for applications and assigning new ones and updating the firewall with them.

key-value config in `/etc/portutils.conf`

```ini
# Run when a public port is removed (remove from firewall)
on-public-port-remove=/path/to/executable
# Run when a public port is added (add to firewall)
on-public-port-add=/path/to/executable

# event executables are run [exe] [port] [service] [description]

# Default padding between ports inside ranges
padding=4
# Path to port.list
map=/etc/port.list

[servicemaps]
default=10000-14999, 20000-39999
web=2000-2999
```

ports map in `/etc/port.list`

Usages:

```
mkport {[ports...]} {[--private]} [service] {[description]}

where ports is a list of ports or port ranges in the format ">x" or "x-y" (all inclusive) to search an open port in.

If no ports are given it will take the default range from servicemaps or a specific one if a service matches.

```
