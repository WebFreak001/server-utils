module config;

import std.algorithm;
import std.ascii;
import std.conv;
import std.datetime.systime;
import std.file;
import std.stdio;
import std.string;

struct PortRange
{
	enum Type
	{
		one,
		more,
		range
	}

	Type type;
	ushort start;
	ushort end;

	static PortRange parse(in char[] s)
	{
		PortRange ret;
		try
		{
			if (s.all!isDigit)
			{
				ret.type = Type.one;
				ret.start = s.to!ushort;
			}
			else if (s.length && s[0] == '>')
			{
				ret.type = Type.more;
				ret.start = s[1 .. $].to!ushort;
			}
			else
			{
				auto parts = s.findSplit("-");
				if (parts[1].length)
				{
					ret.type = Type.range;
					ret.start = parts[0].to!ushort;
					ret.end = parts[2].to!ushort;
				}
			}
		}
		catch (ConvException)
		{
			ret = PortRange.init;
		}
		return ret;
	}
}

alias ServiceMap = PortRange[];

ushort nextPort(ref ServiceMap map, int padding)
{
	if (!map.length)
		return 0;

	auto first = map[0];
	final switch (first.type)
	{
	case PortRange.Type.one:
		map = map[1 .. $];
		return first.start;
	case PortRange.Type.more:
		auto ret = first.start;
		map[0].start += padding;
		return ret;
	case PortRange.Type.range:
		auto ret = first.start;
		map[0].start += padding;
		if (first.start > first.end)
			map = map[1 .. $];
		return ret;
	}
}

struct PortConfig
{
	enum ServiceMap defaultServicemap = [PortRange(PortRange.Type.range, 10000, 39999)];

	string onPublicPortRemove, onPublicPortAdd;
	ushort padding = 4;

	string map = "/etc/port.list";

	ServiceMap[string] servicemaps;
}

PortConfig loadPortConfig(string config = "/etc/portutils.conf")
{
	PortConfig ret;
	if (config.exists)
	{
		string section;
		foreach (line; File(config).byLine)
		{
			line = line.strip;
			if (!line.length)
				continue;
			if (line[0] == ';' || line[0] == '#')
				continue;
			if (line[0] == '[' && line[$ - 1] == ']')
			{
				section = line[1 .. $ - 1].idup;
				continue;
			}

			auto _parts = line.findSplit("=");
			auto key = _parts[0];
			auto value = _parts[2];
			if (section.length == 0)
			{
				switch (key)
				{
				case "on-public-port-remove":
					ret.onPublicPortRemove = value.idup;
					break;
				case "on-public-port-add":
					ret.onPublicPortAdd = value.idup;
					break;
				case "padding":
					if (value.all!isDigit)
					{
						ret.padding = value.to!ushort;
						if (ret.padding == 0)
						{
							stderr.writeln(config, ": Padding can't be 0, changing to 1");
							ret.padding = 1;
						}
					}
					else
					{
						stderr.writeln(config, ": Invalid value for padding, expected number");
					}
					break;
				case "map":
					ret.map = value.idup;
					break;
				default:
					stderr.writeln(config, ": Unknown setting ", key, " in default section");
					break;
				}
			}
			else if (section == "servicemaps")
			{
				auto keyUnsafe = cast(string) key;
				foreach (range; value.splitter(','))
				{
					auto parsed = PortRange.parse(range.strip);
					if (parsed == PortRange.init)
						stderr.writeln(config, ": Invalid port range ", range, " for ",
								key, " (must be of type [port], >[port] or [port]-[port])");
					else if (auto existing = keyUnsafe in ret.servicemaps)
						*existing ~= parsed;
					else
						ret.servicemaps[key.idup] = [parsed];
				}
			}
			else
			{
				stderr.writeln(config, ": Setting in unknown section [", section, "]");
			}
		}
	}
	return ret;
}

struct PortEntry
{
	ushort port;
	SysTime createTime;
	string service;
	string[] links;
	bool public_;
	string description;

	string toString() const
	{
		string str = port.to!string ~ " @ " ~ createTime.toISOExtString ~ " - " ~ service;
		foreach (link; links)
			str ~= " - link:" ~ link;
		if (public_)
			str ~= " - public:yes";
		if (description.length)
			str ~= " - " ~ description;
		return str;
	}

	static PortEntry parse(const(char)[] line)
	{
		try
		{
			line = line.strip;
			PortEntry ret;
			auto parts = line.findSplit("@");
			ret.port = parts[0].strip.to!ushort;
			line = parts[2];
			auto index = line.indexOf(" - ");
			if (index == -1)
				return PortEntry.init;
			ret.createTime = SysTime.fromISOExtString(line[0 .. index].strip);
			line = line[index + 3 .. $].stripLeft;
			index = line.indexOf(" - ");
			if (index == -1)
			{
				ret.service = line.idup;
				return ret;
			}
			ret.service = line[0 .. index].idup;
			line = line[index + 3 .. $].stripLeft;
			while (line.length)
			{
				if (line.startsWith("link:", "public:"))
				{
					index = line.indexOf(" - ");
					const(char)[] attr;
					if (index != -1)
					{
						attr = line[0 .. index];
						line = line[index + 3 .. $];
					}
					else
					{
						attr = line;
						line = null;
					}
					if (attr[0] == 'l')
					{
						auto val = attr["link:".length .. $].strip;
						ret.links ~= val.idup;
					}
					else
					{
						auto val = attr["public:".length .. $].strip;
						ret.public_ = !!val.among!("yes", "true", "on", "1");
					}
				}
				else
				{
					ret.description = line.idup;
					break;
				}
			}
			return ret;
		}
		catch (ConvException)
		{
			return PortEntry.init;
		}
	}
}
